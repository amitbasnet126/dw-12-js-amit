let ar1 = ["a", "b"]
let ar2 = ["x", "y", "z"]

let ar3 = [1, 2, ...ar2, 5, ...ar1] // [1,   2, 'x', 'y','z', 5, 'a', 'b']
console.log(ar3)

// ... (spread operator) are those which open the bracket of an array or simply wrapper opener
// spread operator are used to make new array from the existing array


