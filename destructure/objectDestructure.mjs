// let info = {name:"nitan",age:29,isMarried:false}

//in object variable name must be same while destructuring
let {isMarried,name,age} = {name:"nitan",age:29,isMarried:false}

console.log(name)
console.log(age)
console.log(isMarried)

//while destructuring order doesnot matter