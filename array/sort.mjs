/* 
There are two types of sort
ascending sort means a,b,c,d
descending sort means d,c,b,a
*/

let ar1 = ["a", "c", "b"]

//ascending sort
let ar2 = ar1.sort();
console.log(ar2)


/* 
Interview Questions
1. In cases like [9,10] it sorts the first digit number and if 
it remains same then look after second consecutively then 
which gives sorting of [10, 9]
2. [2,20] it doesnot need to note as one have 1 digit 
    other have two digit
3. ["a", "b", "A"] in this the sorting works by taking
    Upper case first and then lower case which gives
    output ["A", "a", "b"]
*/

