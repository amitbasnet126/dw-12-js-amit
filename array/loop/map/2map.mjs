let ar1 = [1,2,3,4]

/* use map when input and output
both are array and both length are same
for eg
[1,2,3,4] = [2,4,6,7] we can use
[1,2,3,4] = [2,4,6] we cannot use
[1,2,3,4] = "1,2,3,4" we cannot use
*/

let ar2 = ar1.map((value,i) => {
    return value*2
})
console.log(ar2)

