// Define an array
let arr = [1, 2, 3, 4, 5];

// Using array indexing
let lastElement = arr[arr.length - 1];
console.log("Last element using array indexing:", lastElement);

// Using the pop() method (Note: This method modifies the original array)
let poppedElement = arr.pop();
console.log("Last element using pop() method:", poppedElement);
