let names  = ["amit","gopal","arpan"]
//in above "a" starts with index [0], "b" starts with index [1] and "c" ends with [2]

//array is used to store data of different type or same type

//while retrieving all elements in variable
console.log(names)

//while retrieving specific elements in variable
console.log(names[0])
console.log(names[1])
console.log(names[2])

//change elements of array
names[1] = "grish"
console.log(names)