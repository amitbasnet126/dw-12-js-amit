/* 

() //it means parenthesis
{} //it means curly braces or block
[] //it means array 


*/

// if block gets executed if condition is true
if (true){ // inside parenthesis value should either be true or false
    console.log("hello i am if")
}

let name = "ram"
if (name==="nitan"){
    console.log("i am nitan")
}

let a = "0"

if (a){ //in javascript or other programming language it automatically check whether variable is true or false
    console.log("hello")
}