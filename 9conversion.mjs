// 1234 = "1234"
// "1234" = 1234

import { college } from "./8importExport.mjs";

//conversion of numeric to string

console.log(String(1234))// it gives output "1234"

//conversion of string to numeric

console.log(Number("1234")) // it gives output 1234


console.log(Boolean("nitin"));
console.log(Boolean(""))//false
console.log(Boolean("a"))
console.log(Boolean(" "))
console.log(Boolean("0"))
console.log(Boolean(1))
console.log(Boolean(0))//false

/* 


all empty are falsy value

In string
"" => falsy
all are truthy

number
0 => falsy
all are truthy

*/