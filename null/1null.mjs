let a;
a=5
console.log(a)
a=null
console.log(a)

// Undefined means a variable is defined but not touched(initialized)
// Null means a variable is defined and is initialized to empty
 
//An undefined value is not stored in the memory
//A null is stored in the memory as null

// type of null is an object which is bug in Javascript

console.log(typeof(a))
