//let info = ["name", 29, false] we cannot change this array to object because all are values only

let info = [
  ["name", "nitan"],
  ["age", 29],
  ["isMarried", false],
];

let objInfo = Object.fromEntries(info);
console.log(objInfo)
