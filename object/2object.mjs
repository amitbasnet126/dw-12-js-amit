let info = {
  name: "nitan",
  age: 29,
  isMarried: false,
};

//["name","age","isMarried"] these are key
//["nitan",29,false] these are value
//[["name","nitan"],["age",29],["isMarried",false]] these are properties or entries

let keysArray = Object.keys(info);
console.log(keysArray);

let valuesArray = Object.values(info);
console.log(valuesArray);

let propertiesArray = Object.entries(info)
console.log(propertiesArray)