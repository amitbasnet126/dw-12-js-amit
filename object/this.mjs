let info = {
  name: "Amit",
  surName: "Basnet",
  age: 25,
  fullName: function () {
    console.log(`my name is ${this.name} ${this.surName}`);
  },
};

console.log(info.name);
info.fullName();

//this keyword always point itself inside function
//arrow function doesnot support this keyword
