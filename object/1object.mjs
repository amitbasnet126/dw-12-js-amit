// object is used to store multiple data
//object is same as array but it has information of data
//it has 3 part
//property = key + value are the three parts

let info = {
    name: "nitan",
    age:29,
    weight: 65,
    isMarried: false
}

console.log(info.name)
console.log(info.age)
console.log(info.weight)
console.log(info.isMarried)

info.age=30 // to change value

console.log(info)//{name: 'nitan', age: 30, weight: 65, isMarried: false}
delete info.weight
console.log(info) //{name: 'nitan', age: 30, isMarried: false}