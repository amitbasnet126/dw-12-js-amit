console.log("1")
setTimeout(()=>{
    console.log("hello")
}, 0)
console.log("2")
// 1 2 hello


console.log("1")
setTimeout(()=>{
    console.log("hello")
}, 2000)
console.log("2")
//1,2,hello

//Callstack run the code inside it once the code gets executed the code is popped off
//Any code that written in vs code or other IDE of js is stored in callstack by which code will be popped off by line by line except function that will be store in node memory queue
//Anything that push its task to the background(node) are called asynchronous function
//During code execution the background(node) code will execute when all synchronous code(Js code) gets executed

//Event loop is a mediator which continuosly monitor callstack and memory queue if the callstack is empty it push the function from memory queue to call stack